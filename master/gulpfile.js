/******************************
	SETUP
******************************/

/*	VARIABLES
*********************/
var config = require('./gulp.config')();


/*	PLUGINS
*********************/
var gulp        	= require('gulp'),
	plumber     	= require('gulp-plumber'),
	notify      	= require('gulp-notify'),
	run         	= require('run-sequence'),
	sass 			= require('gulp-sass'),
	autoprefixer	= require('gulp-autoprefixer'),
	rename      	= require('gulp-rename'),
	minifyCSS   	= require('gulp-minify-css'),
	concat      	= require('gulp-concat'),
	changed     	= require('gulp-changed'),
	browserSync 	= require('browser-sync').create(),
	uglify      	= require('gulp-uglify'),
	imagemin    	= require('gulp-imagemin'),
	jshint      	= require('gulp-jshint'),
	fs          	= require('fs'),
	path        	= require('path'),
	glob        	= require('glob'),
	merge       	= require('merge-stream'),
	del         	= require('del'),
	exec 			= require('child_process').exec,
	package     	= require('./package.json'); 
	

/*	ERROR HANDLING
************************************************/
var gulp_src = gulp.src;
gulp.src = function() {
	return gulp_src.apply(gulp, arguments)
	.pipe(plumber({ errorHandler: notify.onError({
			title: "<%= error.plugin %>",
			message: "<%= error.message %>"
		})
	}))
};

/******************************
	SUB-TASKS 
******************************/

//	CLEAN: DEV
gulp.task('clean:dev', function(cb){
	del(config.dev+'/*', cb);
});

gulp.task('clean:dev_theme', function(cb){
	del(config.dev+config.wp_theme+'/*', cb);
});


//	CLEAN: DIST
gulp.task('clean:dist', function(cb){
	del(config.dist+'/*', cb);
});

//	CLEAN: THEME
gulp.task('clean:dist_theme', function(cb){
	del(config.dist+config.wp_theme+'/*', cb);
});



//	CLEAN: COMP
gulp.task('clean:comp', function(cb){
	del([
		config.comp+'/*',
		'!'+config.comp+'/**/*.html'
	], cb);
});


//	HTML
gulp.task('html', function () {
	return gulp.src(config.src+'/**/*.html')
	.pipe(changed(config.wf))
	.pipe(gulp.dest(config.wf))
	.pipe(browserSync.reload({stream:true}));
});

gulp.task('php', function () {
	return gulp.src(config.src+config.wp_theme+'/**/*.php')
	.pipe(changed(config.wf+'/**/*'))
	.pipe(gulp.dest(config.wf))
	.pipe(browserSync.reload({stream:true}));
});

gulp.task('php:root', function () {
	return gulp.src(config.src+'/*.php')
	//.pipe(changed(config.wf+'/**/*'))
	.pipe(gulp.dest(config.wf))
	.pipe(browserSync.reload({stream:true}));
});

//	SASS-INCLUDE
//	Import all the componentes files into the file _all.scss.
var sass_includes_runed = false;
gulp.task('sass:includes', function (callback) {
	
	if(sass_includes_runed == true){ callback(); return;}
	sass_includes_runed = true;
	
	var comps_list = '_all';
	var comps_list_path = config.paths.src_scss + '/' + config.comp + '/' +comps_list + '.scss';
	
	fs.writeFile(comps_list_path, '/** This is a dynamically generated file **/\n\n', { overwrite: true }, function (err) {		
		glob(config.paths.src_scss + '/' + config.comp + '/_*.scss', function (error, files) {
			var partials = [];
			files.forEach(function (allFile) {
				var filename = allFile.split('\\').pop().split('/').pop().split('.').shift();
				if ( filename != comps_list ){ 
					partials.push( filename );
				}			
			});
			// Append import statements for each partial
			var import_list = '';
			partials.forEach(function (partial) {
				import_list += '@import "' + partial + '";\n'; 
			});
			fs.appendFileSync(comps_list_path, import_list);
		});
	});
	callback();	
});

//	SCSS
gulp.task('scss', ['sass:includes'], function () {
	
	return gulp.src(config.paths.src_scss+'/styles.scss')
	/*
	.pipe(compass({
		project: __dirname,
		logging: false,
		sourcemap: false,
		import_path: config.paths.sass_includes,
		sass: config.paths.src_scss,
		css: config.wf+config.paths.dest_css
    }))
	*/
	.pipe(sass({
		includePaths: config.paths.sass_includes,
		errLogToConsole: true
	}))
	.pipe(autoprefixer('last 4 version'))
	.pipe(gulp.dest( config.wf + config.paths.dest_css ))
	/*
	.pipe(minifyCSS({
		keepSpecialComments: 1,
		processImport: false
	}))
	.pipe(rename({ suffix: '.min' }))
	.pipe(gulp.dest( config.wf + config.paths.dest_css ))
	*/
	.pipe(browserSync.reload({stream:true}));
	
});


// JAVASCRIPT
gulp.task('js', function(){
	return gulp.src(config.paths.src_js+'/scripts.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(gulp.dest(config.wf+config.paths.dest_js))
    /*
	.pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(config.wf+config.paths.dest_js))
	*/
	.pipe(browserSync.reload({stream:true}));
});


// IMAGES
gulp.task('images', function() {
    return gulp.src( config.files.img )
    .pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
    .pipe(gulp.dest(config.wf+config.paths.dest_img))
	.pipe(browserSync.reload({stream:true}));
});


// VENDORS
gulp.task('vendors', function(){
	return merge(

		// IMAGES
		gulp.src( config.files.vendors.img )
		.pipe(changed( config.wf+config.paths.dest_img ))
		.pipe(gulp.dest( config.wf+config.paths.dest_img )),
		
		//	SCRIPTS
		gulp.src( config.files.vendors.js )
		.pipe(concat('vendors.js', {newLine: ';'}))
		//.pipe(gulp.dest(config.wf+config.paths.dest_js))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(config.wf+config.paths.dest_js)),
		
		// STANDALONE: SCRIPTS
		gulp.src( config.files.vendors.standalone_js )
		.pipe(changed( config.wf+config.paths.dest_js ))
		.pipe(gulp.dest( config.wf+config.paths.dest_js )),
		
		//	CSS
		gulp.src( config.files.vendors.css )
		.pipe(concat('vendors.css'))
		//.pipe(gulp.dest(config.wf+config.paths.dest_css))
		.pipe(minifyCSS({processImport: false}))
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(config.wf+config.paths.dest_css)),
		
		// STANDALONE: STYLES
		gulp.src( config.files.vendors.standalone_css )
		.pipe(changed( config.wf+config.paths.dest_css ))
		.pipe(gulp.dest( config.wf+config.paths.dest_css )),
		
		
		
		//	FONTS
		gulp.src(config.files.vendors.fonts)
		.pipe(changed( config.wf+config.paths.dest_fonts ))
		.pipe(gulp.dest( config.wf+config.paths.dest_fonts ))
	
	); //End merge
});

//	STANDALONE FILES
gulp.task('standalone', function(){
	return merge(
				
		// STANDALONE: SCRIPTS
		gulp.src( config.files.js )
		.pipe(changed( config.wf+config.paths.dest_js ))
		.pipe(gulp.dest( config.wf+config.paths.dest_js )),
		
		// STANDALONE: STYLES
		gulp.src( config.files.css )
		.pipe(changed( config.wf+config.paths.dest_css ))
		.pipe(gulp.dest( config.wf+config.paths.dest_css )),
		
		// STANDALONE: MISC
		gulp.src( config.files.misc )
		.pipe(changed( config.wf  ))
		.pipe(gulp.dest( config.wf ))
					
	); //End merge	
});

//	FONTS
gulp.task('fonts', function () {
    gulp.src( config.files.fonts )
	.pipe(changed( config.wf+config.paths.dest_fonts  ))
	.pipe(gulp.dest( config.wf+config.paths.dest_fonts ))
	.pipe(browserSync.reload({stream:true}));
});


gulp.task('browser-sync', function() {
    browserSync.init(null, {
		notify: false,
        server: {
            baseDir: config.wf,
			directory: config.directory_listing,
        }
    });
});

gulp.task('browser-sync-wp', function() {
 
    //initialize browsersync
    browserSync.init(null, {
		proxy: config.dev_url,
		notify: false
    });
});


gulp.task('bs-reload', function () {
    browserSync.reload();
});


/* LOCAL DATABASE DUMP */
gulp.task('db:dump', function (cb) {

	var date = new Date();
	var today = date.toJSON().slice(2,10);
	var time = date.toJSON().slice(13,19);
	time = (date.getHours() < 10 ? '0' : '') + date.getHours() + time;
	today = today.replace(/\D/g,'');
	time = time.replace(/\D/g,'');
	var fulltime = today + '_' + time;
	
	var cmd =  'mysqldump -u ' + config.database.local.db_user;
	
	if( config.database.db_password ){
		cmd += ' -p' + config.database.local.db_password;
	}

	var backup_name = config.database.local.db_backup_name;
	backup_name = backup_name.replace('@date@', fulltime);
	
	cmd += ' ' + config.database.local.db_name + ' > ' + config.dist + '\\' + backup_name;

  exec( cmd, function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
})

/******************************************
	TASKS
*******************************************/

// DEV
gulp.task('default', ['clean:dev_theme'], function(){
	
	config.directory_listing = false;
	
	config.wf = config.dev;
	run(['php:root'], function(){	
		config.wf = config.dev + config.wp_theme;
		run(['php', 'scss', 'js', 'images', 'fonts', 'vendors', 'standalone', 'browser-sync-wp'], function(){		
			gulp.watch(config.src+config.wp_theme+'/**/*.php', ['php']);
			gulp.watch(config.src+config.wp_theme+'/scss/**/*.scss', ['scss']);
			gulp.watch(config.src+config.wp_theme+'/js/**/*.js', ['js']);
			gulp.watch(config.src+config.wp_theme+'/images/**/*', ['images']);
		});
		
	});
});


//COMPONENTS
gulp.task('comp', ['clean:comp'], function () {
	
	config.directory_listing = true;
	
	config.wf = config.comp;
	run(['scss', 'js', 'images', 'fonts', 'vendors', 'standalone', 'browser-sync'], function () {
		gulp.watch(config.wf + '/**/*.html', ['html']);
		gulp.watch(config.src+config.wp_theme+'/scss/**/*.scss', ['scss']);
		gulp.watch(config.src+config.wp_theme+'/js/**/*.js', ['js']);
		gulp.watch(config.src+config.wp_theme+'/images/**/*', ['images']);
	});	
});


// BUILD
gulp.task('build', ['clean:dist_theme'], function(){
	config.wf = config.dist;
	run(['php:root'], function(){	
		config.wf = config.dist + config.wp_theme;
		run(['php', 'scss', 'js', 'images', 'fonts', 'vendors', 'standalone']);
	});
});

// BUILD
gulp.task('build:full', ['clean:dist'], function(){
	config.wf = config.dist;
	run(['db:dump'], function(){
		config.wf = config.dist;
		run(['php:root'], function(){
			config.wf = config.dist + config.wp_theme;	
			run(['php', 'scss', 'js', 'images', 'fonts', 'vendors', 'standalone']);
		});
	});
});

