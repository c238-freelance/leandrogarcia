<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
include_once 'wp-config-parts.php';
 
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')+D|209%?Z[^qS5j8Xqy,.N~v<>j:E}-;gpiMlLQh{(-zgN}ia)]2=R=Jr5:9KK.');
define('SECURE_AUTH_KEY',  '4[0+slR*|d/+RP.EXhgKDx(X1wm,[U4HLm>y1YkL(U0|Jdxt*TX5vVLEWIH?&}#9');
define('LOGGED_IN_KEY',    '/Wph*Q0el;zH4Hpj067Fq2;fQ`ns>w6hHNCtbTy]pyeM:B+P`C}t/+4&&?c~NHOR');
define('NONCE_KEY',        't0]Lt}4,QEZCc6|lZL*n]W`5 >tZCPuOR+i.wr.M_P7!{Fc3cg+J<cu4okOAu@j7');
define('AUTH_SALT',        'O.`c9n($?|a2QQG5VCy-Op($/ajE0w6^rGZp@_A!.lf;365S+/5?;>K-CQPS{]YD');
define('SECURE_AUTH_SALT', 'gxS)#DIoRvl2;YWiSL-x|^-ANMi%;:D[>Q#Std|gjbmmq.rd%3wAE1C^B-ib7%u|');
define('LOGGED_IN_SALT',   '*pxyntt(?#y@!M7`TTP=8<Wf7H,Dj*=GL-7AjEV ]osnjwi.N/qE|rcm|ut1vXxm');
define('NONCE_SALT',       'VgPA@bM!<xhtz&_D0s-$VuWD?|_pS-H2kBG8A/0:mpicjmnR)U<rC9F`.|aW7a*t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
