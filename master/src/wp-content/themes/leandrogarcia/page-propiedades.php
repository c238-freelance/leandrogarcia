<?php
/*
*	Page: PROPIEDADES
*/
get_header(); ?>
		<div class="wrapper">
			<div id="content">

				<div id="primary-content" class="content-area">

					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<!--
							<header class="entry-header">
								<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							</header>
							-->
							
							<div class="entry-content">
								<?php the_content(); ?>
							</div><!-- .entry-content -->
							
							
							<?php
								$qvar_provincia = get_query_var('provincia') ? get_query_var('provincia') :  'buenos-aires';
								$qvar_zona = get_query_var('zona') ? get_query_var('zona') : false;
								$qvar_partido = get_query_var('partido') ? get_query_var('partido') : false;
								$qvar_localidad = get_query_var('localidad') ? get_query_var('localidad') : false;
								
								$is_zone = $is_partido = $is_localidad = false;
								
								if( $qvar_localidad ){
									$is_localidad = true;
									
									$loc_term = get_term_by('slug', $qvar_localidad, 'zona');
									$loc_id = $loc_term->term_id;
									
									$part_term = get_term_by('slug', $qvar_partido, 'zona');
									$part_id = $part_term->term_id;
									
									$zone_term = get_term_by('slug', $qvar_zona, 'zona');
									$zone_id = $zone_term->term_id;
									
								}else if( $qvar_partido ){
									$is_partido = true;
									
									$part_term = get_term_by('slug', $qvar_partido, 'zona');
									$part_id = $part_term->term_id;
									
									$zone_term = get_term_by('slug', $qvar_zona, 'zona');
									$zone_id = $zone_term->term_id;
									
								}else if( $qvar_zona ){
									$zone_term = get_term_by('slug', $qvar_zona, 'zona');
									$zone_id = $zone_term->term_id;
									$is_zone = true;
								}else{
									$is_zone = true;
								}
							?>
							
							
							<?php
							if( $is_localidad ){ // LOCALIDAD
							?>
							<section id="inmuebles-tipos" class="cmp-zone-list-1">
							
								<header class="cmp-breadcrumb-1">
									<ul class="list-inline">
										<li><span>Buscar</span></li>
										<li><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia), ''); ?>"><span>Provincia de Buenos Aires</span></a></li>
										<li><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $zone_term->slug), ''); ?>"><span>Zona <?php echo $zone_term->name; ?></span></a></li>
										<?php
										if( $part_term ){
										?>
										<li><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $zone_term->slug, 'partido'=> $part_term->slug), ''); ?>"><span>Partido <?php echo $part_term->name; ?></span></a></li>
										<?php
										}
										?>
										<li><span><?php echo $loc_term->name; ?></span></li>
									</ul>
								</header>
								
								<?php 
								// WP_Query arguments
								$args = array (
									'post_type' => 'inmueble',
									'tax_query' => array(
										array(
											'taxonomy' => 'zona',
											'field'    => 'slug',
											'terms'    => $loc_term->slug,
										),
									),
								);

								// The Query
								$query = new WP_Query( $args );
								$props_list= array();
								
								// The Loop
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										
										$post_propiedades = wp_get_post_categories( get_the_ID() ); // Recupero las categorias del post
										
										$propiedades = get_the_terms( get_the_ID(), 'propiedad' );
										foreach ( $propiedades as $prop){
											$props_list[$prop->term_id] =  $prop->name; //Anoto el slug de la categoria
										}
										
									}
									$props_list = array_unique($props_list); //Filtro las categorias repetidas
								} else {
								?>
								<article class="cmp-zone-group-1 cmp-zone-group-1-empty">
									<p>No se encontraron propiedades en esta zona.</p>
									<p><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $zone_term->slug, 'partido'=> $part_term->slug), ''); ?>">Volver a la zona anterior</a></p>
									
								</article>
								<?php
								}
								wp_reset_postdata();
								
								if (count($props_list) ){
								?>
								<section id="inmuebles-propiedades" class="cmp-prop-list-1">
									<?php
									foreach( $props_list as $prop_id => $prop_name){
										$arr_query = array(
											'localidad' 	=> $loc_term->term_id,
											'tipo'	=> $prop_id
										);
										
									?>
									<article class="cmp-prop-item-1" data-type="<?php echo sanitize_title($prop_name); ?>"><a href="<?php echo  get_permalink(38)  . '?' . http_build_query($arr_query); ?>"><span><?php echo $prop_name; ?></span></a></article>
									<?php
									}
									?>
								</section>
								<?php
								}
								// Restore original Post Data
								
								?>
							</section>
							<?php
								
							}else if( $is_partido ){ // PARTIDO
							?>	
							<section id="inmuebles-partidos" class="cmp-zone-list-1">
							
								<header class="cmp-breadcrumb-1">
									<ul class="list-inline">
										<li><span>Buscar</span></li>
										<li><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia), ''); ?>"><span>Provincia de Buenos Aires</span></a></li>
										<li><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $zone_term->slug), ''); ?>"><span>Zona <?php echo $zone_term->name; ?></span></a></li>
									</ul>
								</header>
								
								<?php 
								$args = array(
									'parent' 			=>	$zone_id,	//Just get top level terms
									'hide_empty'	=>	false,
									'include'			=>	$part_id
								); 
								$parent_zones= get_terms('zona', $args);
								foreach( $parent_zones as $key => $pzone){
								?>
								<article class="cmp-zone-group-1">
									<h1><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $zone_term->slug, 'partido'=>$pzone->slug), ''); ?>"><span>Partido <?php echo $pzone->name; ?></span></a></h1>
									<section>
										<?php
										$args = array(
											'parent' 			=>	$pzone->term_id,
											'hide_empty'		=>	false 
										); 
										$children_zones = get_terms('zona', $args);
										
										foreach( $children_zones as $key => $czone){
										?>
											<article>
												<a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona'=> $zone_term->slug, 'partido' => $pzone->slug, 'localidad'=> $czone->slug )); ?>"><span><?php echo $czone->name; ?></span></a>
											</article>												
										<?php
										}
										?>
									</section>
								</article>
								<?php
								}
								?>
							</section>
							<?php
							}else if( $is_zone ){ // ZONA
							?>
							<section id="inmuebles-zonas" class="cmp-zone-list-1">
							
								<header class="cmp-breadcrumb-1">
									<ul class="list-inline">
										<li><span>Buscar</span></li>
										<li><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia), ''); ?>"><span>Provincia de Buenos Aires</span></a></li>
									</ul>
								</header>
								
								<?php 
								$args = array(
									'parent' 			=>	0,	//Just get top level terms
									'hide_empty'	=>	false, 
									'include' => $zone_id
								); 
								$parent_zones= get_terms('zona', $args);
								foreach( $parent_zones as $key => $pzone){
								?>
								<article class="cmp-zone-group-1">
									<h1><a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $pzone->slug)); ?>"><span>Zona <?php echo $pzone->name; ?></span></a></h1>
									<section>
										<?php
										$args = array(
											'parent' 			=>	$pzone->term_id,
											'hide_empty'		=>	false 
										); 
										$children_zones = get_terms('zona', $args);
										
										foreach( $children_zones as $key => $czone){
										?>
											<article>
												<?php
												$termchildren = get_term_children( $czone->term_id, 'zona' );
												if( count($termchildren) ){
												?>
												<a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $pzone->slug, 'partido'=> $czone->slug )); ?>"><span><?php echo $czone->name; ?></span></a>
												<?php
												}else{
												?>
												<a href="<?php echo add_query_arg(array('provincia'=>$qvar_provincia, 'zona' => $pzone->slug, 'localidad'=> $czone->slug )); ?>"><span><?php echo $czone->name; ?></span></a>
												<?php
												}
												?>
												
											</article>												
										<?php
										}
										?>
									</section>
								</article>
								<?php
								}
								?>
							</section>
							<?php
								}
							?>
							
							
							
						</article><!-- #post-## -->
						
					<?php endwhile; // End of the loop. ?>

				</div><!-- #primary-content -->

				<?php //get_sidebar(); ?>
				
			</div><!-- #content -->
		</div><!-- .wrapper -->

<?php get_footer(); ?>
