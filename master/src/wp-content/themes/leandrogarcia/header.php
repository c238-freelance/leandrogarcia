<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package lgarcia
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- STYLES -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" type="text/css" media="screen">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="wrapper">
		
		<a id="hamburger" class="cmp-hamburger-1 visible-xs-block" href="#menu-mobile"><span><span>MENU</span></span></a>
		
		<div class="page-wrap">
	
			<div id="pre-header" class="hidden-xs">
				<div class="wrapper">
					<div class="cmp-open-hours-1 icon-door">Abierto de Lunes a Sábados de 9 a 20 hs</div>
					<div class="cmp-share-links-1">
						Seguinos
						<ul>
							<li class="share-facebook"><a href="#"><span>Facebook</span></a></li>
							<li class="share-twitter"><a href="#"><span>Twitter</span></a></li>
						</ul>	
					</div>
				</div>
			</div>


			<header id="header" role="banner">				
				<div class="wrap">
				
					<div id="logo" class="cmp-logo-1">
						<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
					</div><!-- .site-branding -->
					
					<div class="wrap hidden-xs">
						
						<div class="cmp-tag-meta-1 tag-address">Av. Hipolito Yrigoyen 4243 - 1° F <br> Lanús Oeste - C.P. 1824</div>
						<div class="cmp-tag-meta-1 tag-contact"><span class="meta meta-phone">(+54 11) 3968-8761</span> <br> <a class="meta meta-mail" href="mailto:leandrogarciaprop@gmail.com">leandrogarciaprop@gmail.com</a></div>
					
					</div>
					
					<div id="site-navigation" class="cmp-nav-main-1 hidden-xs" role="navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
					</div><!-- #site-navigation -->
				
				</div>
				
				<section class="cmp-search-filter-1">
					<div class="wrapper">
						<?php get_search_form(); ?>
					</div>
				</section>
			</header><!-- #header -->
			
			<main id="main" role="main">