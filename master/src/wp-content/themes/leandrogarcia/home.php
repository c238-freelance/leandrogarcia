<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package lgarcia
 */

get_header(); ?>
			
			<div id="masthead">
				<div class="wrapper">
					<div class="cmp-slider-1 slick-slider">
						<div><img src="<?php bloginfo('template_url'); ?>/images/slide_1.jpg" alt=""></div>
						<div><img src="<?php bloginfo('template_url'); ?>/images/slide_2.jpg" alt=""></div>
						<div><img src="<?php bloginfo('template_url'); ?>/images/slide_3.jpg" alt=""></div>
						<div><img src="<?php bloginfo('template_url'); ?>/images/slide_4.jpg" alt=""></div>
					</div>
				</div>
			</div>

			<section id="content">
				<div class="wrapper">

					<section id="primary-content" class="content-area">
					
						<div class="cmp-highlight-1">
							<h3 class="title">Servicio integral inmobiliario, jurídico y arquitectónico.</h3>
							<p>Nuestro objetivo, con profesionales letrados del Colegio de Martillero y Corredores Públicos del Departamento Judicial de Avellaneda-Lanús, es resolver de manera profesional, rápida y eficiente cualquier situación derivada de las operaciones inmobiliarias, como tasaciones, sucesiones, ventas, alquileres, escrituras, boletos, asesoramiento impositivo-contable, etc..</p>
						</div>
					
						<section class="cmp-tag-meta-list-2">
						
							<article class="cmp-tag-meta-2 tag-tasacion flipInX animate">
								<a href="<?php echo get_permalink(15); ?>">
									<h1>Tasaciones</h1>
									<div class="content">El valor justo para promover un negocio exitoso.</div>
								</a>
							</article>
							
							<article class="cmp-tag-meta-2 tag-venta flipInX animate">
								<a href="<?php echo get_permalink(38) . "?operacion=venta"; ?>">
									<h1>Ventas</h1>
									<div class="content">Ofrecemos garantía de calidad en la comercialización de propiedades.</div>
								</a>
							</article>
							
							<article class="cmp-tag-meta-2 tag-alquiler flipInX animate">
								<a href="<?php echo get_permalink(38) . "?operacion=alquiler"; ?>">
									<h1>Aquileres</h1>
									<div class="content">Una ampllia cartera de clientes se beneficia con nuestro servicio de administración.</div>
								</a>
							</article>							

							
						</section>
						
						
					</section><!-- #primary-content -->


				</div><!-- .wrapper -->
			</section><!-- #content -->

<?php get_footer(); ?>
