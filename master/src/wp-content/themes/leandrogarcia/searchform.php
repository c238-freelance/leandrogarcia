<?php
	//Recovery of search query variables.
	
	//OPERATION TYPES
	$fields = get_field_object('field_562be1d5a7555');
	$operation_types = $fields['choices'];

	//CURRENCY TYPES
	$fields = get_field_object('field_562be200a7556');
	$currency_types = $fields['choices'];
?>
	<form class="wrap" action="<?php echo get_permalink(38); ?>" method="get">
		
		<h1><span>Busqueda rápida de propiedades</span></h1>
		
		
	
		<div class="wrap">
			<div class="form-group">
				<label>Operación:</label>
				<select name="operacion">
					<option value="0">Operaciones</option>
				<?php
				//echo $_GET['operacion'];
					$selected_op_type = $_GET['operacion'] ? sanitize_key($_GET['operacion']) : false;
					foreach( $operation_types as $type => $name ){
				?>	
					<option value="<?php echo $type;?>"<?php echo ($selected_op_type==$type) ? ' selected="selected"': '';?>><?php echo $name;?></option>
				<?php
					}
				?>
				
				</select>
			</div>
				
			<div class="form-group">
				<label>Moneda:</label>
				<select name="moneda">
					<option value="0">Monedas</option>
				<?php
					$selected_currency_type = $_GET['moneda'] ? sanitize_key($_GET['moneda']) : false;
					foreach( $currency_types as $type => $name ){
				?>	
					<option value="<?php echo $type;?>"<?php echo ($selected_currency_type==$type) ? ' selected="selected"': '';?>><?php echo $name;?></option>
				<?php
					}
				?>
				
				</select>
			</div>
			
			<div class="form-group group-rango form-inline">
				<div class="form-group">
					<label>Desde:</label>
					<input type="number" min="0" step="100" name="desde" class="form-control" placeholder="Desde" value="<?php echo ($_GET['desde']) ? sanitize_key($_GET['desde']) : ''; ?>" title="Desde">
				</div>
			
				<div class="form-group">
					<label>Hasta:</label>
					<input type="number" min="0" step="100"  name="hasta" class="form-control" placeholder="Hasta" value="<?php echo ($_GET['hasta']) ? sanitize_key($_GET['hasta']) : ''; ?>" title="Hasta">
				</div>							
			</div>
			
			<div class="form-group">
				<label>Localidad:</label>
				 <?php 
					$selected_zona = $_GET['localidad'] ? sanitize_key($_GET['localidad']) : false;
					$args = array(
						'name' => 'localidad',
						'selected' => $selected_zona,
						'taxonomy'           => 'zona',
						'hide_empty'      => false,
						'orderby' => 'name',
						'hierarchical' => 1,
						//'show_option_all' => 'Localidades'//,
						'show_option_none' => 'Localidades',
						'option_none_value' => 0
					);
					wp_dropdown_categories( $args );
				 ?>
			</div>
				
			<div class="form-group">
				<label>Propiedad:</label>
				<?php 
					$selected_tipo = $_GET['tipo'] ? sanitize_key($_GET['tipo']) : false;
					$args = array(
						'name' => 'tipo',
						'selected' => $selected_tipo,
						'taxonomy'           => 'propiedad',
						'hide_empty'      => false,
						'orderby' => 'name',
						'hierarchical' => 1,
						//'show_option_all' => 'Propiedades'//,
						'show_option_none' => 'Propiedades',
						'option_none_value' => 0
					);
					wp_dropdown_categories( $args );
				 ?>
			</div>
			
			<div class="form-group">		
				<button type="submit" class="btn cmp-btn-1">Buscar</button>
			</div>
			
			
			
		</div>	
	</form>
	