<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package lgarcia
 */

?>
		</div><!-- .page-wrap -->

		<footer id="footer">
				
				<div class="foot wrap">
					
					<div class="cmp-logo-1">
						<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
					</div>
					
					<div class="tag-list">
						<ul >
							<li><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-map-marker fa-stack-1x fa-inverse"></i></span> Hipolito Yrigoyen 4243</li>
							<li><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-phone fa-stack-1x fa-inverse"></i></span><a href="tel:(+5411) 4115 5689">(+5411) 4115 5689</a></li>
							<li><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-stack-1x fa-inverse">@</i></span><a href="mailto:info@leandrogarcia.com.ar">info@leandrogarcia.com.ar</a></li>
						</ul>
					</div>
					
					<div class="cmp-share-links-2">
						Seguinos
						<ul class="list-inline">
							<li class="share-facebook"><a href="#"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span><span class="hide">Facebook</span></a></li>
							<li class="share-twitter"><a href="#"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span><span class="hide">Twitter</span></a></li>
						</ul>	
					</div>
					
					<div class="cmp-data-fiscal-1">
						<div class="thumb"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/data_fiscal.jpg" alt=""></a></div>
					</div>
					
				</div>
				

				
			
			<div class="sub-footer">
					
					<div class="foot">
						<div class="copy">&copy; Copyright 2015 | Leandro García</div>
						<div class="by">
							<ul class="list-inline">
								<li class="design">Diseño por <a href="mailto:viviana_vago@hotmail.com ">Viviana Vago</a></li>
								<li class="development">Desarrollo por <a href="http://www.c238.com.ar/">C238</a></li>
							</ul>
						</div>
					</div><!-- .foot -->

			</div>
			
		</footer><!-- #footer-->
			
	</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>
