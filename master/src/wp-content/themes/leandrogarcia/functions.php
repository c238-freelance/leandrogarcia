<?php
/**
 * lgarcia functions and definitions
 *
 * @package lgarcia
 */

if ( ! function_exists( 'lgarcia_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function lgarcia_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on lgarcia, use a find and replace
	 * to change 'lgarcia' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'lgarcia', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'lgarcia' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'lgarcia_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	
	
}
endif; // lgarcia_setup
add_action( 'after_setup_theme', 'lgarcia_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function lgarcia_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'lgarcia_content_width', 640 );
}
add_action( 'after_setup_theme', 'lgarcia_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function lgarcia_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'lgarcia' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'lgarcia_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function lgarcia_scripts() {
	//wp_enqueue_style( 'lgarcia-style', get_stylesheet_uri() );
	wp_enqueue_script( 'bootstrap3-scripts', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', array('lgarcia-vendors-scripts') );
	
	wp_enqueue_script( 'google-maps-scripts', '//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=es-AR', array('jquery') );
	
	wp_enqueue_style( 'lgarcia-vendors-style', get_template_directory_uri() . '/css/vendors.min.css' );
	wp_enqueue_script( 'lgarcia-vendors-scripts', get_template_directory_uri() . '/js/vendors.min.js', array('jquery') );
	
	wp_enqueue_style( 'lgarcia-style', get_template_directory_uri() . '/css/styles.css', array('lgarcia-vendors-style') );
	wp_enqueue_script( 'lgarcia-scripts', get_template_directory_uri() . '/js/scripts.js' , array('bootstrap3-scripts', 'lgarcia-vendors-scripts', 'google-maps-scripts'));

	//wp_enqueue_script( 'lgarcia-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	//wp_enqueue_script( 'lgarcia-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		//wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'lgarcia_scripts' );


function dashboard_css() {
   wp_enqueue_style('fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', '', '4.4.0', 'all');
}
add_action('admin_init', 'dashboard_css');


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



/**
 * IMAGES SIZES
 */
add_image_size( 'inm-thumb', 260, 342, array('center', 'center') );
add_image_size( 'staff-thumb', 195, 271, array('center', 'center') );


// Register Custom Taxonomy
function custom_taxonomy_zonas() {

	$labels = array(
		'name'                       => _x( 'Zonas', 'Taxonomy General Name', 'lgarcia' ),
		'singular_name'              => _x( 'Zona', 'Taxonomy Singular Name', 'lgarcia' ),
		'menu_name'                  => __( 'Zonas', 'lgarcia' ),
		'all_items'                  => __( 'Zonas', 'lgarcia' ),
		'parent_item'                => __( 'Zona padre', 'lgarcia' ),
		'parent_item_colon'          => __( 'Zona padre:', 'lgarcia' ),
		'new_item_name'              => __( 'Nueva zona', 'lgarcia' ),
		'add_new_item'               => __( 'Agregar zona', 'lgarcia' ),
		'edit_item'                  => __( 'Editar zona', 'lgarcia' ),
		'update_item'                => __( 'Actualizar zona', 'lgarcia' ),
		'view_item'                  => __( 'Ver zona', 'lgarcia' ),
		'separate_items_with_commas' => __( 'Separar zonas con comas', 'lgarcia' ),
		'add_or_remove_items'        => __( 'Agregar o quitar zonas', 'lgarcia' ),
		'choose_from_most_used'      => __( 'Elegir entre las zonas más usadas', 'lgarcia' ),
		'popular_items'              => __( 'Zonas pupulares', 'lgarcia' ),
		'search_items'               => __( 'Buscar zonas', 'lgarcia' ),
		'not_found'                  => __( 'Ninguna zona encontrada', 'lgarcia' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'zona', array( 'inmueble' ), $args );

}
add_action( 'init', 'custom_taxonomy_zonas', 0 );

// Register Custom Taxonomy
function custom_taxonomy_propiedad() {

	$labels = array(
		'name'                       => _x( 'Propiedades', 'Taxonomy General Name', 'lgarcia' ),
		'singular_name'              => _x( 'Propiedad', 'Taxonomy Singular Name', 'lgarcia' ),
		'menu_name'                  => __( 'Propiedades', 'lgarcia' ),
		'all_items'                  => __( 'Todas las propiedades', 'lgarcia' ),
		'parent_item'                => __( 'Propiedad padre', 'lgarcia' ),
		'parent_item_colon'          => __( 'Propiedad padre:', 'lgarcia' ),
		'new_item_name'              => __( 'Nueva propiedad', 'lgarcia' ),
		'add_new_item'               => __( 'Agregar una nueva propiedad', 'lgarcia' ),
		'edit_item'                  => __( 'Editar propiedad', 'lgarcia' ),
		'update_item'                => __( 'Actualizar propiedad', 'lgarcia' ),
		'view_item'                  => __( 'Ver propiedad', 'lgarcia' ),
		'separate_items_with_commas' => __( 'Separar items con comas', 'lgarcia' ),
		'add_or_remove_items'        => __( 'Agregar o quitar propiedades', 'lgarcia' ),
		'choose_from_most_used'      => __( 'Elije una de entre las más usadas', 'lgarcia' ),
		'popular_items'              => __( 'Propiedades populares', 'lgarcia' ),
		'search_items'               => __( 'Buscar propiedades', 'lgarcia' ),
		'not_found'                  => __( 'Ninguna propiedad encontrada', 'lgarcia' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'propiedad', array( 'inmueble' ), $args );

}
add_action( 'init', 'custom_taxonomy_propiedad', 0 );

// Register Custom Post Type
function custom_post_type_inmuebles() {

	$labels = array(
		'name'                => _x( 'Inmuebles', 'Post Type General Name', 'lgarcia' ),
		'singular_name'       => _x( 'Inmueble', 'Post Type Singular Name', 'lgarcia' ),
		'menu_name'           => __( 'Inmuebles', 'lgarcia' ),
		'name_admin_bar'      => __( 'Inmuebles', 'lgarcia' ),
		'parent_item_colon'   => __( 'Inmueble padre:', 'lgarcia' ),
		'all_items'           => __( 'Todos los inmuebles', 'lgarcia' ),
		'add_new_item'        => __( 'Agregar un inmueble', 'lgarcia' ),
		'add_new'             => __( 'Agrear un inmueble', 'lgarcia' ),
		'new_item'            => __( 'Nuevo inmueble', 'lgarcia' ),
		'edit_item'           => __( 'Editar inmueble', 'lgarcia' ),
		'update_item'         => __( 'Actualizar inmueble', 'lgarcia' ),
		'view_item'           => __( 'Ver inmueble', 'lgarcia' ),
		'search_items'        => __( 'Buscar inmueble', 'lgarcia' ),
		'not_found'           => __( 'Inmueble no encontrado', 'lgarcia' ),
		'not_found_in_trash'  => __( 'Inmueble no encontrado en la papelera', 'lgarcia' ),
	);
	$args = array(
		'label'               => __( 'Inmueble', 'lgarcia' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt'),
		'taxonomies'          => array( 'zona' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'inmueble', $args );

}
add_action( 'init', 'custom_post_type_inmuebles', 0 );




// Register Custom Post Type
function custom_post_type_staff() {



	$labels = array(
		'name'                => _x( 'Staff members', 'Post Type General Name', 'lgarcia' ),
		'singular_name'       => _x( 'Staff member', 'Post Type Singular Name', 'lgarcia' ),
		'menu_name'           => __( 'Staff members', 'lgarcia' ),
		'name_admin_bar'      => __( 'Staff members', 'lgarcia' ),
		'parent_item_colon'   => __( 'Staff member padre:', 'lgarcia' ),
		'all_items'           => __( 'Todos los miembros', 'lgarcia' ),
		'add_new_item'        => __( 'Agregar un miembro', 'lgarcia' ),
		'add_new'             => __( 'Agrear un miembro', 'lgarcia' ),
		'new_item'            => __( 'Nuevo miembro', 'lgarcia' ),
		'edit_item'           => __( 'Editar miembro', 'lgarcia' ),
		'update_item'         => __( 'Actualizar miembro', 'lgarcia' ),
		'view_item'           => __( 'Ver miembro', 'lgarcia' ),
		'search_items'        => __( 'Buscar miembro', 'lgarcia' ),
		'not_found'           => __( 'Staff member no encontrado', 'lgarcia' ),
		'not_found_in_trash'  => __( 'Staff member no encontrado en la papelera', 'lgarcia' ),
	);
	$args = array(
		'label'               => __( 'Staff member', 'lgarcia' ),
		'labels'              => $labels,
		'supports'            => array('title'),
		'taxonomies'          => array(),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'staff', $args );
	
	
}
add_action( 'init', 'custom_post_type_staff', 0 );


/*my custom title label*/
add_filter( 'enter_title_here', 'custom_enter_title' );

function custom_enter_title( $input ) {
	global $post_type;

	if ('staff' == $post_type ){ 
		return __( 'Nombre del miembro del staff');
	}

	return $input;
}


// Add Shortcode Ex: [staff id="22"]
function staff_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'id' => '',
		), $atts )
	);
	
	$output = '';
	
	// WP_Query arguments
	$args = array (
		'post_type'		=> array( 'staff' ),
		'post_status'	=> array( 'publish' ),
		'orderby'		=> 'title',
		'order'			=> 'ASC'
	);

	// The Query
	$members = new WP_Query( $args );

	// The Loop
	if ( $members->have_posts() ) {
		
		$output .= '<div class="cmp-staff-list-1"><div class="row">';
	
		while ( $members->have_posts() ) {
			$members->the_post();
			
			$image = get_field('staff-image');
			
			$output .= '<div class="col-sm-6">
							<div class="cmp-staff-1">
								<div class="thumb">'. (is_array($image) ? wp_get_attachment_image( $image['id'], 'staff' ) : '') .'</div> 
								<h1>'. get_the_title() .'</h1>
								<div class="description">'. get_field('staff-description') .'</div>
							</div>
						</div>';
		}
		
		$output .= '</div></div><!-- /.cmp-staff-list-1 -->';
		
		
	}

	// Restore original Post Data
	wp_reset_postdata();
	
	
	
	return $output;
	
}
add_shortcode( 'staff', 'staff_shortcode' );




/*	CUSTOM POST MENU ICONS
******************************************************/

function wp_admin_custom_post_menu_icons() {
   echo '
   <style type="text/css" media="screen">
		#adminmenu #menu-posts-inmueble .wp-menu-image:before {
			font-family: fontawesome;
            content: "\f1ad";
        }
		#adminmenu #menu-posts-staff .wp-menu-image:before {
			font-family: fontawesome;
            content: "\f0c0";
        } 		
    </style>';
}
add_action('admin_head', 'wp_admin_custom_post_menu_icons');



//	ADD CUSTOM VARS TO URL QUERY FOR PAGE PROPIEDADES
//*******************************************************/
function custom_query_vars_filter($vars) {
	$vars[] = 'provincia';
	$vars[] .= 'zona';
	$vars[] .= 'partido';
	$vars[] .= 'localidad';
	$vars[] .= 'propiedad';
	return $vars;
}
add_filter( 'query_vars', 'custom_query_vars_filter' );



function home_page_menu_args( $args ) {
$args['show_home'] = true;
return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );



function get_image_sizes( $size = '' ) {

        global $_wp_additional_image_sizes;

        $sizes = array();
        $get_intermediate_image_sizes = get_intermediate_image_sizes();

        // Create the full array with sizes and crop info
        foreach( $get_intermediate_image_sizes as $_size ) {

                if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

                        $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
                        $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
                        $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

                } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

                        $sizes[ $_size ] = array( 
                                'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                                'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
                        );

                }

        }

        // Get only 1 size if found
        if ( $size ) {

                if( isset( $sizes[ $size ] ) ) {
                        return $sizes[ $size ];
                } else {
                        return false;
                }

        }

        return $sizes;
}