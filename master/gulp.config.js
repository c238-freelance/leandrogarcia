module.exports = function () {  
	var package = require('./package.json');
	
	var config = {
		src:		'src', //Sources
		dev:		'dev',	//Develop
		dist:		'dist',	//Distribution
		comp:	'components',	//Components develop
		wp_theme: '/wp-content/themes/'+package.name, //Get the theme name from the package.json file
		directory_listing: false
	};
	config.dev_url = "http://dev.lgarcia.local/";
	
	config.dist_theme = config.dist + config.wp_theme; //Distribution theme folder
	
	config.wf = config.dev; //Default Working Folder

	config.paths = {
		src_css: 			config.src + config.wp_theme + '/css',
		src_scss: 			config.src + config.wp_theme + '/scss',
		src_js: 				config.src + config.wp_theme + '/js',
		src_img:			config.src + config.wp_theme + '/images',
		src_fonts:			config.src + config.wp_theme + '/fonts',
		src_vendors: 		config.src + config.wp_theme + '/vendors',
		
		dest_css: 			'/css',
		dest_js: 			'/js',
		dest_img:			'/images',
		dest_fonts:		'/fonts'
	};
	
	//MySQL Database Dump
	config.database = {
		local: {
			db_name:		'c238_lgarcia',
			db_user:		'root',
			db_password:	'',
			db_host:		'localhost',
			db_backup_name:	'@date@_local_backup.sql' //Use @date@ as a repalce target for date
		}
	}
	
	config.paths.sass_includes = [ 
		config.paths.src_vendors + '/bootstrap-sass/assets/stylesheets/'
	];
	
	config.files = {
		img: [
			config.paths.src_img +'/**/*',
			'!' + config.paths.src_img +'/**/*.psd'
		],
		css: [
			config.paths.src_css +'/*.css'
		],
		js: [
			config.paths.src_js + '/*.js',
			'!' + config.paths.src_js + '/scripts.js'
		],
		fonts: [
			config.paths.src_fonts + '/*'
		],
		misc: [
			config.src + config.wp_theme + '/style.css',
			config.src + config.wp_theme + '/rtl.css',
			config.src + config.wp_theme + '/screenshot.png'
		],
		vendors: {
			img: [],
			css: [
				config.paths.src_vendors + '/jQuery.mmenu/dist/core/css/jquery.mmenu.all.css',
				config.paths.src_vendors + '/jQuery.mmenu/dist/extensions/css/jquery.mmenu.themes.css',
				config.paths.src_vendors + '/jQuery.mmenu/dist/extensions/css/jquery.mmenu.pageshadow.css',
				config.paths.src_vendors + '/animate.css/animate.css',
				config.paths.src_vendors + '/slick.js/slick/slick.css',
				config.paths.src_vendors+ '/slick.js/slick/slick-theme.css',
				config.paths.src_vendors + '/bootstrap-select/dist/css/bootstrap-select.css'
			],
			js: [
				config.paths.src_vendors + '/jquery/dist/jquery.js',
				config.paths.src_vendors + '/wow/dist/wow.js',
				config.paths.src_vendors + '/jQuery.mmenu/dist/core/js/umd/jquery.mmenu.umd.all.js',
				config.paths.src_vendors + '/slick.js/slick/slick.js',
				config.paths.src_vendors + '/better-input-file/dist/betterInputFileButton.js',
				config.paths.src_vendors + '/bootstrap-select/dist/js/bootstrap-select.js'
			],
			fonts: [
				config.paths.src_vendors + '/bootstrap-sass/assets/fonts/bootstrap/*',
				config.paths.src_vendors + '/slick.js/slick/fonts/*'
			],
			standalone_img: [
			],
			standalone_css: [
				config.paths.src_vendors + '/slick.js/slick/ajax-loader.gif'
			],
			standalone_js: [
				config.paths.src_vendors + '/html5shiv/dist/html5shiv.min.js'
			]
		}
	};
	
    return config;
};




