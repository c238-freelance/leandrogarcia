<?php
/**
 * The template for displaying "inmueble" single posts.
 *
 * @package lgarcia
 */

get_header(); ?>
		<div class="wrapper">
			<div id="content">

				<div id="primary-content" class="content-area">
				
				
					<?php
						$zonas_list = '';
						
						$arr_zona = wp_get_post_terms( get_the_ID(), 'zona' );
						$arr_tipo = wp_get_post_terms( get_the_ID(), 'propiedad' );
						
						if( $arr_zona[0]->term_id ){
							$zonas_ancestors = get_ancestors( $arr_zona[0]->term_id, 'zona');
							if( count($zonas_ancestors) ){
								foreach( $zonas_ancestors as $key => $ancestor_id){
									$ancestor = get_term($ancestor_id, 'zona');
									$zonas_list = '<li><a href="'. add_query_arg(array('localidad'=>$ancestor_id), '') .'"><span>Zona '. $ancestor->name .'</span></a></li>' . $zonas_list;
								}
							}
							$zona = get_term($arr_zona[0]->term_id, 'zona');
							$zonas_list .= '<li><a href="'. add_query_arg(array('localidad'=>$_GET['localidad']), '') .'"><span>'. $zona->name .'</span></a></li>';
						}
						
						if( $arr_tipo[0]->term_id ){
							$tipo = get_term($arr_tipo[0]->term_id, 'propiedad');
							$zonas_list .= '<li class="term-tipo"><a href="'. get_permalink(38) . '?localidad=' . $arr_tipo[0]->term_id .'"><span>'. $tipo->name .'</span></a></li>';
						}						
					?>
					
					<section id="inmuebles-resultados" class="cmp-inmuebles-list-1">
						<header class="cmp-breadcrumb-1">
							<ul class="list-inline">
								<li><span>Buscar</span></li>
								<li><a href="<?php echo add_query_arg(array('localidad'=>''), ''); ?>"><span>Provincia de Buenos Aires</span></a></li>
								<?php echo $zonas_list; ?>
							</ul>
						</header>
					

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'template-parts/content', 'inmueble' ); ?>

						<?php endwhile; // End of the loop. ?>
					</section>

				</div><!-- #primary-content -->

				<?php //get_sidebar(); ?>

			</div><!-- #content -->
		</div><!-- .wrapper -->

<?php get_footer(); ?>
