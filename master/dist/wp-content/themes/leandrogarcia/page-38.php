<?php
/**
 * The template for displaying search results pages.
 *
 * @package lgarcia
 */

get_header(); ?>
		<div class="wrapper">
			<div id="content">

				<div id="primary-content" class="content-area">
					
					<?php
						$zonas_list = '';
						if( isset($_GET['localidad']) && $_GET['localidad'] != ''){
							$zonas_ancestors = get_ancestors( $_GET['localidad'], 'zona');
							if( count($zonas_ancestors) ){
								foreach( $zonas_ancestors as $key => $ancestor_id){
									$ancestor = get_term($ancestor_id, 'zona');
									$zonas_list = '<li><a href="'. add_query_arg(array('localidad'=>$ancestor_id), '') .'"><span>Zona '. $ancestor->name .'</span></a></li>' . $zonas_list;
								}
							}
							$zona = get_term($_GET['localidad'], 'zona');
							$zonas_list .= '<li><a href="'. add_query_arg(array('localidad'=>$_GET['localidad']), '') .'"><span>'. $zona->name .'</span></a></li>';
						}
						
						if( isset($_GET['tipo']) && $_GET['tipo'] != ''){
							$tipo = get_term($_GET['tipo'], 'propiedad');
							$zonas_list .= '<li class="term-tipo"><a href="'. add_query_arg(array('tipo'=>$_GET['tipo']), '') .'"><span>'. $tipo->name .'</span></a></li>';
						}						
					?>
					
					<section id="inmuebles-resultados" class="cmp-inmuebles-list-1">
						<header class="cmp-breadcrumb-1">
							<ul class="list-inline">
								<li><span>Buscar</span></li>
								<li><a href="<?php echo add_query_arg(array('localidad'=>''), ''); ?>"><span>Provincia de Buenos Aires</span></a></li>
								<?php echo $zonas_list; ?>
							</ul>
						</header>
					
						<?php
							//Recovery of search query variables.
							
							
							
						
							//Retrive all published inmuebles.
							$args = array (
								'post_type'		=> array( 'inmueble' ),
								'post_status'	=> array( 'Publish' ),
								'tax_query' 		=> array(),
								'meta_query'	=> array(
									'relation' 	=> 'AND'
								)
							);
							
							//Filter by operatino type
							if( isset($_GET['operacion']) && $_GET['operacion'] != ''){
								$args['meta_query'][] = array(
									'key'     		=> 'inm_operacion',
									'value'   		=> $_GET['operacion'],
									'compare' 	=> '='
								);
							}
							
							//Filter by currency type
							if( isset($_GET['moneda']) && $_GET['moneda'] != ''){
								$args['meta_query'][] = array(
									'key'     		=> 'inm_moneda',
									'value'   		=> $_GET['moneda'],
									'compare'	=> '='
								);
							}
							
							//Filter by price range
							if( isset($_GET['desde']) && $_GET['desde'] != ''){
								$args['meta_query'][] = array(
									'key'     		=> 'inm_precio',
									'value'   		=> $_GET['desde'],
									'compare' 	=> '>'
								);
							}
							
							//Filter by price range
							if( isset($_GET['hasta']) && $_GET['hasta'] != ''){
								$args['meta_query'][] = array(
									'key'     		=> 'inm_precio',
									'value'   		=> $_GET['hasta'],
									'compare' 	=> '<'
								);
							}
							
							//Filter by operation
							if( isset($_GET['tipo']) && $_GET['tipo'] != ''){
								$args['tax_query'][] = array(
									'taxonomy' 	=> 'propiedad',
									'field' 		=> 'id',
									'terms' 		=> array( $_GET['tipo'])
								);
							} 
							
							//Filter by zone
							if( isset($_GET['localidad']) && $_GET['localidad'] != ''){
								$args['tax_query'][] = array(
									'taxonomy'	 => 'zona',
									'field' 		=> 'id',
									'terms' 		=> array( $_GET['localidad'])
								);
							} 
							
							//Filter by type
							if( isset($_GET['tipo']) && $_GET['tipo'] != ''){
								$args['tax_query'][] = array(
									array(
										'taxonomy'	=> 'propiedad',
										'field' 		=> 'id',
										'terms' 		=> array( $_GET['tipo'])
									)
								);
							} 

							// The Query
							$inmuebles = new WP_Query( $args );

							// The Loop
							if ( $inmuebles->have_posts() ) {			
							?>
							<div class="wrap">
							<?php 
								$article_count = 0;
								while ( $inmuebles->have_posts() ) {
									$inmuebles->the_post();
									get_template_part( 'template-parts/content', 'list-inmueble' );
									if( $article_count % 2 == 1 ){
										echo '<div class="clearfix"></div>';
										
									}
									$article_count++;
								}
								?>
							</div>
							<?php
								the_posts_navigation();
							} else {
								get_template_part( 'template-parts/content', 'none' );
							}

						// Restore original Post Data
						wp_reset_postdata();
						?>
					</section>
				
				</div><!-- #primary-content -->

				
			</div><!-- #content -->
		</div><!-- .wrapper -->

<?php get_footer(); ?>
