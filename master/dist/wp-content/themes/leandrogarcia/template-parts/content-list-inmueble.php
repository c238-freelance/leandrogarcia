<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package lgarcia
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cmp-inmueble-item-1'); ?>>
	
	
	<div class="thumb fadeIn animate">
		<?php
		$images = get_field('inm_galeria');
		
		if( $images ){ ?>
		<div class="cmp-slider-2 slick-slider">
			<?php
			foreach( $images as $image ){
			?>
			<div>
				<?php echo wp_get_attachment_image( $image, 'inm-thumb', 0, array('class'=>'img-responsive img-rounded') ); ?>
			</div>			 
			<?php } ?>
		</div>
		<?php }else{
			
			$inm_thumb_size = get_image_sizes('inm-thumb');
		?>
			<img src="http://placehold.it/<?php echo $inm_thumb_size['width'] . 'x' .$inm_thumb_size['height']; ?>&text=no image" class="img-responsive img-rounded">
		<?php
		}
		?>
	</div><!-- .thumb -->
	
	<div class="content">
		<header class="entry-header">
			<div class="inmueble-tipos">
				<ul class="list-inline">
					<?php
					$terms = get_the_terms( $post->ID, 'propiedad' );
											
					if ( $terms && ! is_wp_error( $terms ) ){
						$terms_list = '';
						foreach ( $terms as $term ) {							
							$terms_list .= '<li><a href="'. add_query_arg(array('tipo'=>$term->term_id), '') .'">'.$term->name.'</a></li>';
						}
						echo $terms_list;
					}
					?>
				</ul>
			</div><!-- .entry-meta -->
			<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		</header><!-- .entry-header -->

		<div class="cmp-inmueble-meta-1">
			<ul>				
				<?php 
				//SUPERFICIE
				if( have_rows('inm_superficie_m2') ){
					$field = get_field_object('field_561c33c5082b1');
					$label = $field['label'];

					$sub_field = get_sub_field_object('field_561c358843837');//Tipo baño
					$metros_count = 0;
					$superficie_list = '';
					while ( have_rows('inm_superficie_m2') ){
						the_row();
						
						$sub_tipo_value = get_sub_field('inm_superficie_tipo');
						$sub_tipo_name = $sub_field['choices'][$sub_tipo_value];
						$sub_tipo_cantidad = get_sub_field('inm_superficie');
						
						$superficie_list .= '<li>'. $sub_tipo_cantidad .'m<sup>2</sup> '. $sub_tipo_name . '</li>';

						$metros_count += $sub_tipo_cantidad;
					}					
				?>
				<li class="meta-<?php echo strtolower ($label); ?>"><label><?php echo $label; ?>:</label> <?php echo $metros_count; ?>m<sup>2</sup>
					<ul>
						<?php echo $superficie_list; ?>
					</ul>
				</li>
				<?php
				}
				?>
				
				<?php
				//DORMITORIOS
				$value = get_field('inm_dormitorios');
				if( $value ){
				?>
				<li class="meta-dormitorios"><label>Dormitorios:</label> <?php echo $value; ?></li>
				<?php
				}
				?>
				
				
				<?php 
				//BAÑOS
				if( have_rows('inm_baños') ){
					$field = get_field_object('field_561c2e5b082a3');
					$label = $field['label'];
					
					$sub_field = get_sub_field_object('field_562be89697fce');//Tipo baño
					//print_r($sub_field);
					$banos_count = 0;
					$banos_list = '';
					while ( have_rows(inm_baños) ){
						the_row();
						
						$sub_tipo_value = get_sub_field('inm-bano_tipo');
						$sub_tipo_name = $sub_field['choices'][ $sub_tipo_value ];
						$sub_tipo_cantidad = get_sub_field('inm_bano_cantidad');
						
						$banos_list .= '<li>'. $sub_tipo_cantidad .' '. $sub_tipo_name . '</li>';
						$banos_count += $sub_tipo_cantidad;
					}					
				?>
				<li class="meta-banos"><label><?php echo $label; ?>:</label> <?php echo $banos_count; ?>
					<ul>
						<?php echo $banos_list; ?>
					</ul>
				</li>
				<?php
				}
				?>
				
				
				
				<?php
				//VISTA
				$value = get_field('inm_vista');
				if($value){
					$field = get_field_object('field_561c3491082b4');
					$label = $field['label'];	
					$name = $field['choices'][ $value ];
				?>
				<li class="meta-<?php echo strtolower ($label); ?>"><label><?php echo $label; ?>:</label> <?php echo $name; ?></li>
				<?php
				}
				?>
				
				
				<?php
				//ANTIGUEDAD
				$field = get_field_object('field_561c2d610e3d9');
				$value = $field['value'];
				if($value){
					$value = date('Y') - date('Y', $value); //Años de antiguedad
					$label = $field['label'];
				?>
				<li class="meta-<?php echo strtolower ($label); ?>"><label><?php echo $label; ?>:</label> <?php echo $value . _n(' año', ' años', $value); ?></li>
				<?php
				}
				?>
				
				
				<?php
				// COCHERA
				?>
				<li class="meta-cochera"><label>Cochera:</label> <?php echo (get_field('inm_cochera')) ? 'Si': 'No'; ?></li>
				
				<?php
				//ESTADO
				$value = get_field('inm_estado');
				if($value){
					$field = get_field_object('field_561c312c082a9');
					$label = $field['label'];
					$choice = $field['choices'][ $value ];
				?>
				<li class="meta-<?php echo strtolower ($label); ?>"><label><?php echo $label; ?>:</label> <?php echo $choice; ?></li>
				<?php
				}
				?>
				
				<?php 
				//AMENITIES
				$field = get_field_object('field_561c2d070e3d8');
				$label = $field['label'];
				$value = $field['value'];
				$choices = $field['choices'];

				if( $value ){ ?>
				<li class="meta-amenities"><label>Amenities:</label>
					<ul>
					<?php foreach( $value as $item ){ ?>
					<li>
						<?php echo $choices[ $item ]; ?>
					</li>
					<?php } ?>
					</ul>
				</li>
			<?php
			}
			?>
				
				
			</ul>
		</div>
	
	</div>

	<footer class="entry-footer">
		<a href="<?php the_permalink();?>" class="btn cmp-btn-2">más INFO</a>
	</footer><!-- .entry-footer -->
		
</article><!-- #post-## -->

