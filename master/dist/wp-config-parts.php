<?php 
/*
*   Multi-Environment Config Part for WordPress
*   By lucas.dasso@c238.com.ar
*
* Determines the current domain hosting, and calls
* the corresponding configuration file, defined by
* the "WP_ENV" constant.
*
* Ex.: Config file part name => config.[CP_ENV].php
*
*/
define('HOSTNAME', $_SERVER['SERVER_NAME']);

switch (HOSTNAME) {
    case 'leandrogarcia.c238.com.ar': //Stage
        define('CP_ENV', 'stage');
        break;

    default: 
        define('CP_ENV', 'local');
        break;
} 
include_once('wp-config-parts.'. CP_ENV .'.php');